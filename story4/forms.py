from django.db import models
from django import forms
from .models import PortfolioModel
import datetime

class PortfolioForm(forms.ModelForm):
    title = forms.CharField(widget=forms.TextInput(
        attrs = {
                "class" : "form-control",
                "placeholder" : "Project's Title",
                }
    ))
    category = forms.CharField(widget=forms.TextInput(
        attrs = {
                "class" : "form-control",
                "placeholder" : "Project's Category",
                }
    ))
    description = forms.CharField(widget=forms.TextInput(
        attrs = {
                "class" : "form-control",
                "placeholder" : "Project's Description",
                }
    ))

    place = forms.CharField(widget=forms.TextInput(
        attrs = {
                "class" : "form-control",
                "placeholder" : "Project's Place",
                }
    ))

    start_date =  forms.DateField(input_formats=['%Y-%m-%d'], initial=datetime.date.today(),widget=forms.TextInput(attrs=
                                {
                                    'class':'form-control'
                                }))

    end_date =  forms.DateField(input_formats=['%Y-%m-%d'], initial=datetime.date.today(),widget=forms.TextInput(attrs=
                                {
                                    'class':'form-control'
                                }))

    class Meta:
        model = PortfolioModel
        fields = ("title","start_date","end_date","description","place","category")
        labels = {
            "title":"Project Title",
            "start_date":"Start Date",
            "end_date":"End Date",
            "description":"Description",
            "place":"Place",
            "category":"Category"
        }