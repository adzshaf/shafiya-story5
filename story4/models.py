from django.db import models

# Create your models here.

class PortfolioModel(models.Model):
    title = models.CharField(max_length=100)
    start_date = models.DateField()
    end_date = models.DateField()
    description = models.CharField(max_length=200)
    place = models.CharField(max_length=50)
    category = models.CharField(max_length=50)