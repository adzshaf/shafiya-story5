from django.shortcuts import render
from datetime import datetime
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from .models import PortfolioModel
from .forms import PortfolioForm


waktu = datetime.now()

# Create your views here.

def home(request):
    return render(request,'page/index.html', {'current_date':waktu})

def index(request):
    return render(request, 'page/index.html', {'current_date':waktu})

def blog(request):
    return render(request,'page/blog.html', {'current_date':waktu})

def signup(request):
    return render(request,'page/signup.html', {'current_date':waktu})

def show(request):
    data = PortfolioModel.objects.all() # Ambil data dari database
    return render(request,'page/form_result.html',{"data":data})

def form(request):
    if request.method == "POST":
        data = PortfolioForm(request.POST)
        if data.is_valid():
            data.save() # save ke database
        else:
           print(data.errors)
    return render(request,'page/form.html',{"form":PortfolioForm()})
    return redirect("/page/form_result.html")
